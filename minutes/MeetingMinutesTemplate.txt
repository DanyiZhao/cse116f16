Meeting Minutes for Sep. 8th, 2016:

* Meeting Attendance: Danyi(M),Erick(M),Fatemeh(M),Junjie(M) [1 minute]
    * Put an 'M' next to the name of the student taking minutes
    * Put a 'U' next to the name of the student updating user stories
* User stories completed since last meeting [1-2 minutes]
    0
    * List the name/label of each user story that was completed in the last week
    * Some weeks, including the first week your group meets, this will be blank
* User stories worked upon but not completed since last meeting [2-4 minutes]
We played the board game "CLUE" and understood how the game is working.
    * List each of these stories by name/label
    * Significant discussions of ideas or actions to make progress on these stories should be documented in your 'userStory' directory
    * Some weeks, including the first week your group meets, this will be blank
* Goals for the next meeting [20+ minutes]
    We will sketch the outline for the project.
    * Choose which of the remaining user stories will be worked upon in the coming week.
    * Discuss what is needed to complete each of these user stories.
    * Large user stories will need to be broken into a number of smaller user stories. For example, each user story in Stage #1 requires a lot of preparatory work AND include multiple potential situations. Different parts of the preparatory work and each possible situation that needs solving can be made into their own, much smaller, user story. Document these changes in your 'userStory' folder.
* Schedule for the next week's set of pair programming meetings [3-4 minutes]
    NEXT meeting will take place in the upcoming recitation.
    * Pair programming sessions should be two hours long (no more, no less)
    * Every team member should participate in two sessions each week, with each session including a different team member
    * Identify the user story to be worked on in each session

    
  Danyi(DIDN'T SHOW ),Erick(M),Fatemeh(M),Junjie(M)
  
  Everyone except for Danyi showed up for 1 hour every Tuesday after recitation.
  
  Everyone except for Danyi Met 5 times for 4 hours each time Between Phase 1 and phase 2 submissions