package GUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Random;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

public class MainGui extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public int checkifLost=0;
	
	private String Player = " Solution is Below   ";
	private int input;
	public int[] losers=new int[6];
	public int totLosers=0;
	private Object user;
	private JLabel _instruction;
	public static int WINDOW_WIDTH = 1100;
	public static int WINDOW_HEIGHT = 700;
	private final int PADDING = 5;
	private final int SIDE_LENGTH = 27;
	private final int BUTTON_HEIGHT = 50;
	private final int LABEL_HEIGHT = 15;
	private final int INPUT_HEIGHT = 50;
	int _phase = 0;
	JTextField myField = new JTextField();
	
	
	// private validplace vp = new validplace();

	// private validplace validmove;
	private String HandCard1 = "Prof.Plum \n \n";
	private String HandCard2 = "Kitchen \n \n";
	private String HandCard3 = "Rope \n \n";
	

	private JTextArea jPlayertxtArea = new JTextArea();

	public MainGui() {
		super("Clude Alpha0.1");// titles
		this.setSize(1300, 900);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);

		JPanel Playertxt = new JPanel();

		jPlayertxtArea.append(" Player: " + Player + "'s Turn");

		jPlayertxtArea.append("\n \n" + "\n \n Cards in Hand:" + "\n" + HandCard1 + " ," + HandCard2 + " ," + HandCard3);
		jPlayertxtArea.append(result);

		jPlayertxtArea.setFont(jPlayertxtArea.getFont().deriveFont(20f));
		// JLabel Turntxt= new JLabel("Player: " + Player + "'s Turn" );

		Playertxt.add(jPlayertxtArea);

		JPanel panel = new JPanel(new GridBagLayout());
		// JPanel panel2 = new JPanel();// Haven't use
		JPanel imagepanel = new JPanel(null);

		ImageIcon upimage = new ImageIcon("Image/up.jpg");
		ImageIcon downimage = new ImageIcon("Image/down.jpg");
		ImageIcon leftimage = new ImageIcon("Image/left.jpg");
		ImageIcon rightimage = new ImageIcon("Image/right.jpg");
		ImageIcon simage = new ImageIcon("Image/dice.jpg");
		ImageIcon diceimage = new ImageIcon("Image/end.jpg");
		ImageIcon make_suggest = new ImageIcon("Image/finger.jpg");

		JButton up = new JButton(upimage);
		// up.setPreferredSize(new Dimension(25, 25));

		JButton down = new JButton(downimage);
		JButton left = new JButton(leftimage);
		JButton right = new JButton(rightimage);
		JButton _dice = new JButton(simage);
		JButton Miss_Peacock = new JButton("Miss Peacock");
		JButton secondbutton = new JButton("Knife");
		JButton third = new JButton("hi3");
		JButton qua = new JButton("hi4");
		JButton end_turn = new JButton();
		end_turn.setText("NEXT");
		end_turn.setPreferredSize(new Dimension(100, 100));
		
		
		JButton make_suggestion = new JButton("ACCUSATION");
		make_suggestion.setPreferredSize(new Dimension(150, 100));
		JPanel newpanel = new JPanel();
		newpanel.add(Miss_Peacock);
		newpanel.add(secondbutton);
		newpanel.add(third);
		newpanel.add(qua);

		JLabel player = new JLabel("player");

		JButton player1 = new JButton();
		player1.setBackground(Color.BLACK);

		JButton player2 = new JButton("B");
		player2.setBackground(Color.BLUE);
		JButton player3 = new JButton("C");
		player3.setBackground(Color.GREEN);
		JButton player4 = new JButton("D");
		player4.setBackground(Color.WHITE);
		JButton player5 = new JButton("E");
		player5.setBackground(Color.YELLOW);
		JButton player6 = new JButton("F");
		player6.setBackground(Color.RED);
		///////////////////////////////
		JButton Prof_Plum = new JButton();
		Prof_Plum.setBackground(Color.BLACK);
		JButton Mrs_Peacock = new JButton("B");
		Mrs_Peacock.setBackground(Color.BLUE);
		JButton Miss_Scarlet = new JButton("C");
		Miss_Scarlet.setBackground(Color.GREEN);
		JButton Mrs_White = new JButton("D");
		Mrs_White.setBackground(Color.WHITE);
		JButton Mr_Green = new JButton("E");
		Mr_Green.setBackground(Color.PINK);
		JButton Col_Musturd = new JButton("F");
		Col_Musturd.setBackground(Color.RED);

		GridBagConstraints Button = new GridBagConstraints();
		Button.gridx = 2;
		Button.gridy = 0;
		Button.ipadx = 0;

		panel.add(up, Button);
		Button.gridx = 2;
		Button.gridy = 1;
		Button.ipadx = 0;

		panel.add(down, Button);
		Button.gridx = 1;
		Button.gridy = 1;
		Button.ipadx = 0;

		panel.add(left, Button);
		Button.gridx = 3;
		Button.gridy = 1;
		Button.ipadx = 0;

		panel.add(right, Button);
		Button.gridx = 4;
		Button.gridy = 1;

		panel.add(_dice, Button);
		Button.gridx = 7;
		Button.gridy = 0;

		panel.add(end_turn, Button);
		Button.gridx = 7;
		Button.gridy = 0;

		panel.add(make_suggestion);

		panel.add(player, Button);

		imagepanel.add(Prof_Plum);
		Prof_Plum.setBounds(50, 145, 25, 25);
		imagepanel.add(Mrs_Peacock);
		Mrs_Peacock.setBounds(50, 470, 25, 25);
		imagepanel.add(Miss_Scarlet);
		Miss_Scarlet.setBounds(533, 15, 25, 25);
		imagepanel.add(Mrs_White);
		Mrs_White.setBounds(473, 620, 25, 25);
		imagepanel.add(Mr_Green);
		Mr_Green.setBounds(320, 620, 25, 25);
		imagepanel.add(Col_Musturd);
		Col_Musturd.setBounds(753, 193, 25, 25);
		ImageIcon board = new ImageIcon("Image/board.jpg");
		JLabel imLabel = new JLabel(board);
		imLabel.setPreferredSize(new Dimension(board.getIconWidth(), board.getIconHeight()));
		imagepanel.add(imLabel);
		imLabel.setBounds(-10, -130, 800, 900);

		_instruction = new JLabel("");
		_instruction.setSize(WINDOW_WIDTH - (WINDOW_HEIGHT - PADDING * 2) - PADDING * 2, LABEL_HEIGHT);
		_instruction.setLocation(WINDOW_HEIGHT - PADDING * 2 + PADDING * 2, PADDING * 3);

		secondbutton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				test7();
			}

			private void test7() {
				System.out.println("You Pressed down");
			}

		});

		make_suggestion.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				createAndShowGUI();
				
			}
		});

		_dice.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				diceRoll();
			}

			public int diceRoll() {
				Random rand = new Random();

				int diceNum = rand.nextInt(5) + 2;
				// 6 is the maximum and the 2 is our minimum

				jPlayertxtArea.append("\n" + "You rolled  " + diceNum);
				return diceNum;
			}

		});

		down.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				move();
			}

			public void move() {

				int y = +20;
				if (Player == "Miss Scarlet") {
					Miss_Scarlet.setBounds(Miss_Scarlet.getX(), Miss_Scarlet.getY() + y, Miss_Scarlet.getWidth(),
							Miss_Scarlet.getHeight());
				} else {
					if (Player == "Professor Plum") {
						Prof_Plum.setBounds(Prof_Plum.getX(), Prof_Plum.getY() + y, Prof_Plum.getWidth(),
								Prof_Plum.getHeight());
					} else {
						if (Player == "Mrs. White") {
							Mrs_White.setBounds(Mrs_White.getX(), Mrs_White.getY() + y, Mrs_White.getWidth(),
									Mrs_White.getHeight());
						} else {
							if (Player == "Mrs. Peacock") {
								Mrs_Peacock.setBounds(Mrs_Peacock.getX(), Mrs_Peacock.getY() + y,
										Mrs_Peacock.getWidth(), Mrs_Peacock.getHeight());
							} else {
								if (Player == "Col. Mustard ") {
									Col_Musturd.setBounds(Col_Musturd.getX(), Col_Musturd.getY() + y,
											Col_Musturd.getWidth(), Col_Musturd.getHeight());
								} else {
									if (Player == "Mr. Green") {
										Mr_Green.setBounds(Mr_Green.getX(), Mr_Green.getY() + y, Mr_Green.getWidth(),
												Mr_Green.getHeight());
									}
								}
							}
						}
					}
				}
			}
		}

		);

		left.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				test4();
			}

			private void test4() {
				int x = -20;
				if (Player == "Miss Scarlet") {
					Miss_Scarlet.setBounds(Miss_Scarlet.getX() + x, Miss_Scarlet.getY(), Miss_Scarlet.getWidth(),
							Miss_Scarlet.getHeight());
				} else {
					if (Player == "Professor Plum") {
						Prof_Plum.setBounds(Prof_Plum.getX() + x, Prof_Plum.getY(), Prof_Plum.getWidth(),
								Prof_Plum.getHeight());
					} else {
						if (Player == "Mrs. White") {
							Mrs_White.setBounds(Mrs_White.getX() + x, Mrs_White.getY(), Mrs_White.getWidth(),
									Mrs_White.getHeight());
						} else {
							if (Player == "Mrs. Peacock") {
								Mrs_Peacock.setBounds(Mrs_Peacock.getX() + x, Mrs_Peacock.getY(),
										Mrs_Peacock.getWidth(), Mrs_Peacock.getHeight());
							} else {
								if (Player == "Col. Mustard ") {
									Col_Musturd.setBounds(Col_Musturd.getX() + x, Col_Musturd.getY(),
											Col_Musturd.getWidth(), Col_Musturd.getHeight());
								} else {
									if (Player == "Mr. Green") {
										Mr_Green.setBounds(Mr_Green.getX() + x, Mr_Green.getY(), Mr_Green.getWidth(),
												Mr_Green.getHeight());
									}
								}
							}
						}
					}
				}
			}
		}

		);

		right.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				test5();
			}

			private void test5() {
				int x = +20;
				if (Player == "Miss Scarlet") {
					Miss_Scarlet.setBounds(Miss_Scarlet.getX() + x, Miss_Scarlet.getY(), Miss_Scarlet.getWidth(),
							Miss_Scarlet.getHeight());
				} else {
					if (Player == "Professor Plum") {
						Prof_Plum.setBounds(Prof_Plum.getX() + x, Prof_Plum.getY(), Prof_Plum.getWidth(),
								Prof_Plum.getHeight());
					} else {
						if (Player == "Mrs. White") {
							Mrs_White.setBounds(Mrs_White.getX() + x, Mrs_White.getY(), Mrs_White.getWidth(),
									Mrs_White.getHeight());
						} else {
							if (Player == "Mrs. Peacock") {
								Mrs_Peacock.setBounds(Mrs_Peacock.getX() + x, Mrs_Peacock.getY(),
										Mrs_Peacock.getWidth(), Mrs_Peacock.getHeight());
							} else {
								if (Player == "Col. Mustard ") {
									Col_Musturd.setBounds(Col_Musturd.getX() + x, Col_Musturd.getY(),
											Col_Musturd.getWidth(), Col_Musturd.getHeight());
								} else {
									if (Player == "Mr. Green") {
										Mr_Green.setBounds(Mr_Green.getX() + x, Mr_Green.getY(), Mr_Green.getWidth(),
												Mr_Green.getHeight());
									}
								}
							}
						}
					}
				}
			}

		}

		);

		up.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				int y = -20;
				if (Player == "Miss Scarlet") {
					Miss_Scarlet.setBounds(Miss_Scarlet.getX(), Miss_Scarlet.getY() + y, Miss_Scarlet.getWidth(),
							Miss_Scarlet.getHeight());
				} else {
					if (Player == "Professor Plum") {
						Prof_Plum.setBounds(Prof_Plum.getX(), Prof_Plum.getY() + y, Prof_Plum.getWidth(),
								Prof_Plum.getHeight());
					} else {
						if (Player == "Mrs. White") {
							Mrs_White.setBounds(Mrs_White.getX(), Mrs_White.getY() + y, Mrs_White.getWidth(),
									Mrs_White.getHeight());
						} else {
							if (Player == "Mrs. Peacock") {
								Mrs_Peacock.setBounds(Mrs_Peacock.getX(), Mrs_Peacock.getY() + y,
										Mrs_Peacock.getWidth(), Mrs_Peacock.getHeight());
							} else {
								if (Player == "Col. Mustard ") {
									Col_Musturd.setBounds(Col_Musturd.getX(), Col_Musturd.getY() + y,
											Col_Musturd.getWidth(), Col_Musturd.getHeight());
								} else {
									if (Player == "Mr. Green") {
										Mr_Green.setBounds(Mr_Green.getX(), Mr_Green.getY() + y, Mr_Green.getWidth(),
												Mr_Green.getHeight());
									}
								}
							}
						}
					}
				}
			}
		});
		end_turn.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				if(checkifLost==1){
					checkifLost=0;
					if(input>0){
						input=input-1;
					}
					else{
						input=5;
					}
					
					if(input== 0)
						Miss_Scarlet.setVisible(false);
						
					if(input== 1)
						Prof_Plum.setVisible(false);
					
					if(input== 2)
						Mr_Green.setVisible(false);
					if(input== 3)
						Mrs_White.setVisible(false);
					if(input== 4)
						Mrs_Peacock.setVisible(false);
					if(input== 5)
						Col_Musturd.setVisible(false);
					input++;
				}
				playerTurn();
				jPlayertxtArea.setText("");
				jPlayertxtArea.append(" Player: " + Player + "'s Turn");
				jPlayertxtArea.append("\n" + "\n" + "\n" + "Cards in Hand:" + "\n" + HandCard1 + " ," + "\n" + HandCard2
						+ " ," + "\n" + HandCard3);
				jPlayertxtArea.append(result);
				System.out.println(Player);

			}
		});

		this.add(panel, BorderLayout.PAGE_END);
		this.add(newpanel, BorderLayout.EAST);
		this.add(jPlayertxtArea, BorderLayout.LINE_END);
		this.add(_instruction);
		this.add(imagepanel);
		this.setVisible(true);
		// GRIDING

		SwingUtilities.updateComponentTreeUI(this);

	}
	
	public String playerTurn() {
		if (input > 5) {
			input = 0;
		}
		for(int i=0;i<totLosers;i++){
			if(input==losers[i])
				input++;
		}
		for(int i=0;i<totLosers;i++){
			if(input==losers[i])
				input++;
		}
		for(int i=0;i<totLosers;i++){
			if(input==losers[i])
				input++;
		}
		for(int i=0;i<totLosers;i++){
			if(input==losers[i])
				input++;
		}
		switch (input) {
		
		case 0:

			Player = "Miss Scarlet";
			input += 1;
			HandCard1 = "Mrs. White";
			HandCard2 = "Dagger";
			HandCard3 = "Conservatory";
			break;
		case 1:
			Player = "Professor Plum";
			input += 1;
			HandCard1 = "Miss Scarlet";
			HandCard2 = "Wrench";
			HandCard3 = "Library";
			break;
		case 2:
			Player = "Mr. Green";
			input += 1;
			HandCard1 = "Col Musturd";
			HandCard2 = "Candlestick";
			HandCard3 = "BallRoom";
			break;
		case 3:
			Player = "Mrs. White";
			input += 1;
			HandCard1 = "Mr. Green";
			HandCard2 = "Pistol";
			HandCard3 = "Dining Room";
			break;
		case 4:
			Player = "Mrs. Peacock";
			input += 1;
			HandCard1 = "Mrs. White";
			HandCard2 = "Dagger";
			HandCard3 = "Lounge";
			break;
		case 5:
			Player = "Col. Mustard ";
			input += 1;
			HandCard1 = "Billiard Room";
			HandCard2 = "Study";
			HandCard3 = "Hall";
			break;

		}
		// solution Plum, Rope,Kitchen
		return Player;
	}

	public void grabSuggestionInput() {
		_phase = 0;
		
		this.setEnabled(true);
		this.jPlayertxtArea.append("Please enter the weapon.");
	}

	final static boolean shouldFill = true;
	final static boolean shouldWeightX = true;
	final static boolean RIGHT_TO_LEFT = false;
	private static String[] answer;
	private static String weaponCard;
	private static String roomCard;
	private static String personCard;
	private static String result;
	
	// solution
    final static String player_answer = "Professor Plum";
	final static String weapon_answer = "Rope";
	//final static String room_answer = true;
	
	public void addComponentsToPane(Container pane) {
		if (RIGHT_TO_LEFT) {
			pane.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
		}
		ImageIcon submit_image = new ImageIcon("Image/submit.jpg");
		JButton _submit = new JButton(submit_image);

		// JButton button;
		pane.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridx = 1;
		c.gridy =4;
		pane.add(_submit,c);
		
		/**
		if (shouldFill) {
			// natural height, maximum width
			c.fill = GridBagConstraints.HORIZONTAL;
		}
		**/

		JComboBox person = new JComboBox();
		person.addItem("people");
		person.addItem("Miss Scarlet");
		person.addItem("Mr. Green");
		person.addItem("Mrs. Peacock");
		person.addItem("Colonel Mustard");
		person.addItem("Mrs. White");
		person.addItem("Professor Plum");
		person.setBackground(Color.red);
		person.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (person.getSelectedItem().toString().equals("Miss Scarlet")) {
					System.out.println("Miss Scarlet");
					personCard = "Miss Scarlet";

				} else if (person.getSelectedItem().toString().equals("Mr. Green")) {
					System.out.println("Mr. Green");
					personCard = "Mr. Green";
				} else if (person.getSelectedItem().toString().equals("Mrs. Peacock")) {
					System.out.println("Mrs. Peacock");
					personCard = "Mrs. Peacock";
				} else if (person.getSelectedItem().toString().equals("Colonel Mustard")) {
					System.out.println("Colonel Mustard");
					personCard = "Colonel Mustard";
				} else if (person.getSelectedItem().toString().equals("Mrs. White")) {
					System.out.println("Mrs. White");
					personCard = "Mrs. White";
				} else {
					System.out.println("Professor Plum");
					personCard = "Professor Plum";
				}
				
			}
		});
		if (shouldWeightX) {
			c.weightx = 0.5;
		}
		c.fill = GridBagConstraints.HORIZONTAL;
		c.gridx = 0;
		c.gridy = 0;
		pane.add(person, c);

		JComboBox room = new JComboBox();
		room.addItem("room");
		room.addItem("Conservatory");
		room.addItem("Kitchen");
		room.addItem("Ballroom");
		room.addItem("Billiard Room");
		room.addItem("Library");
		room.addItem("Study");
		room.addItem("Dining Room");
		room.addItem("Lounge");
		room.addItem("Hall");
		room.setBackground(Color.orange);
		room.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (room.getSelectedItem().toString().equals("Conservatory")) {
					System.out.println("Conservatory");
					roomCard = "Conservatory";

				} else if (room.getSelectedItem().toString().equals("Kitchen")) {
					System.out.println("Kitchen");
					roomCard = "Kitchen";
				} else if (room.getSelectedItem().toString().equals("Ballroom")) {
					System.out.println("Ballroom");
					roomCard = "Ballroom";
				} else if (room.getSelectedItem().toString().equals("Billiard Room")) {
					System.out.println("Billiard Room");
					roomCard = "Billiard Room";
				} else if (room.getSelectedItem().toString().equals("Library")) {
					System.out.println("Library");
					roomCard = "Library";

				} else if (room.getSelectedItem().toString().equals("Study")) {
					System.out.println("Study");
					roomCard = "Study";

				} else if (room.getSelectedItem().toString().equals("Dining Room")) {
					System.out.println("Dining Room");
					roomCard = "Dining Room";

				} else if (room.getSelectedItem().toString().equals("Lounge")) {
					System.out.println("Lounge");
					roomCard = "Lounge";
				} else {
					System.out.println("Hall");
					roomCard = "Hall";
				}

			}
		});
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 0.5;
		c.gridx = 1;
		c.gridy = 0;
		pane.add(room, c);

		JComboBox weapon = new JComboBox();
		weapon.addItem("weapon");
		weapon.addItem("Candlestick");
		weapon.addItem("Knife");
		weapon.addItem("Rope");
		weapon.addItem("Revolver");
		weapon.addItem("Lead Pipe");
		weapon.addItem("Wrench");
		weapon.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (weapon.getSelectedItem().toString().equals("Candlestick")) {
					System.out.println("Candlestick");
					weaponCard = "Candlestick";

				} else if (weapon.getSelectedItem().toString().equals("Knife")) {
					System.out.println("Knife");
					weaponCard = "Knife";
				} else if (weapon.getSelectedItem().toString().equals("Rope")) {
					System.out.println("Rope");
					weaponCard = "Rope";
				} else if (weapon.getSelectedItem().toString().equals("Revolver")) {
					System.out.println("Revolver");
					weaponCard = "Revolver";
				} else {
					System.out.println("Wrench");
					weaponCard = "Wrench";

				}

			}
		});
		
		_submit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
			compare();
			
			}

		});
		// printAll();
		// System.out.println(weapon.);

		room.setBackground(Color.green);
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 0.5;
		c.gridx = 2;
		c.gridy = 0;
		pane.add(weapon, c);
	}

	private void createAndShowGUI() {
		// Create and set up the window.
		JFrame frame = new JFrame("GridBagLayoutDemo");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		// Set up the content pane.
		addComponentsToPane(frame.getContentPane());

		// Display the window.
		frame.pack();
		frame.setVisible(true);
	}

	public void compare() {
		System.out.println(personCard + weaponCard + roomCard);
		
		if(personCard=="Professor Plum" && weaponCard=="Rope" && roomCard=="Kitchen"){
			System.out.println("YOU HAVE WON THE GAME!!!!!");
			JOptionPane.showMessageDialog(null, "CONGADULATION!, YOU WON!");
			//result ="YOU HAVE WON THE GAME!!!!!";
		}else{
			checkifLost=1;
			losers[totLosers]=input;
			totLosers++;
			
			
			
			//result = "GAME OVER:( ";
			//JOptionPane.showMessageDialog(this, "Eggs are not supposed to be green.");
			JOptionPane.showMessageDialog(null, "The Game Is Over!!");}
			// System.out.println("Why would you pick that, YOU HAVE LOST THE GAME. "+"\n"+ "Please don't become a detective :( ");
			
	}

	public void main(String[] args) {
		// Schedule a job for the event-dispatching thread:
		// creating and showing this application's GUI.
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});

	}

	//

}