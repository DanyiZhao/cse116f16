cardsInHand.txt
Explain how to see the cards in Miss Scarlet (the first player) hand when the program begins;
Explain how to see the cards in another player's hand when their turn begins;
Explain how to verify each card (other than those in the solution) appear in exactly one players hand.

run gui and Press end turn to begin.

the right panel will display the cards in the players hand and will change depending on whose turn it is. so miss scarlet can only
see her cards during her turn and so on.

to verify each card (other than those in the solution) appear in exactly one players hand you can go through all the players turns and see 
no card repeats and the solution cards // solution Plum, Rope, Courtyard
