package edu.buffalo.cse116;


/***
 * Class that defines a 24x25 grid of Strings. It also includes method to allow a
 * programmer to set the value within each location in the grid, return an array
 * 
 */
public class Board2 {
	/**
	 * The 2-d array in which the values in Board are stored.
	 */
	public Object[][] Board;
	public Object curPos;
	public int Xdes;
	public int Ydes;
	Object Col_Musturd_CurPos = Board[23][7];
	public Board2() {

		/**
		 * Create the new Board, but leave it with the default values (0).
		 */
		Board = new String[23][24];
		curPos =null;

	}
/**
 * this method check whether a player is on a Door Tile
 * @return
 */
	public boolean checkDoor() {
		
		
		

		// Door to Conservatory
		if (Board[Xdes][Ydes] == new Room(5)) {// changing destination to the
												// door
			Xdes = 5;
			Ydes = 19;
		}
		// Door to Billiard
		if (Board[Xdes][Ydes] == new Room(6)) {
			Xdes = 6;
			Ydes = 15;
		}
		// Door to Library
		if (Board[Xdes][Ydes] == new Room(7)) {
			Xdes = 7;
			Ydes = 8;
		}
		// Door to Study
		if (Board[Xdes][Ydes] == new Room(8)) {
			Xdes = 6;
			Ydes = 4;
		}
		// Door to Hall the room
		if (Board[Xdes][Ydes] == new Room(0)) {
			Xdes = 11;
			Ydes = 7;
		}
		// Door to Lounge
		if (Board[Xdes][Ydes] == new Room(1)) {
			Xdes = 17;
			Ydes = 6;
		}
		// Door to Dining Room
		if (Board[Xdes][Ydes] == new Room(2)) {
			Xdes = 15;
			Ydes = 12;
		}
		// Door to Kitchen
		if (Board[Xdes][Ydes] == new Room(3)) {
			Xdes = 19;
			Ydes = 17;
		}
		// Door to Ballroom
		if (Xdes == 7 && Ydes == 19 || Xdes == 9 && Ydes == 16 || Xdes == 14 && Ydes == 16
				|| Xdes == 16 && Ydes == 19) {
			Board[Xdes][Ydes] = new Room(6);
			return true;
		}
		return false;
	}

	public boolean checkMoveFisibility(int Xcurrent, int Ycurrent, int Xdes, int Ydes, int diceNum) {
		if (Board[Xdes][Ydes] == null) {
			return false;
		}

		// checking secret passage
		if (Board[Xdes][Ydes] == "Study" && Board[Xcurrent][Ycurrent] == "Kitchen") {
			return true;
		}
		if (Board[Xdes][Ydes] == "Kitchen" && Board[Xcurrent][Ycurrent] == "Study") {
			return true;
		}
		if (Board[Xdes][Ydes] == "Conservatory" && Board[Xcurrent][Ycurrent] == "Lounge") {
			return true;
		}
		if (Board[Xdes][Ydes] == "Lounge" && Board[Xcurrent][Ycurrent] == "Conservatory") {
			return true;
		}

		////////////////////////////

		// Door to Conservatory
		if (Board[Xdes][Ydes] == new Room(5)) {// changing destination to the
												// door
			Xdes = 5;
			Ydes = 19;
		}
		// Door to Billiard
		if (Board[Xdes][Ydes] == new Room(6)) {
			Xdes = 6;
			Ydes = 15;
		}
		// Door to Library
		if (Board[Xdes][Ydes] == new Room(7)) {
			Xdes = 7;
			Ydes = 8;
		}
		// Door to Study
		if (Board[Xdes][Ydes] == new Room(8)) {
			Xdes = 6;
			Ydes = 4;
		}
		// Door to Hall the room
		if (Board[Xdes][Ydes] == new Room(0)) {
			Xdes = 11;
			Ydes = 7;
		}
		// Door to Lounge
		if (Board[Xdes][Ydes] == new Room(1)) {
			Xdes = 17;
			Ydes = 6;
		}
		// Door to Dining Room
		if (Board[Xdes][Ydes] == new Room(2)) {
			Xdes = 15;
			Ydes = 12;
		}
		// Door to Kitchen
		if (Board[Xdes][Ydes] == new Room(3)) {
			Xdes = 19;
			Ydes = 17;
		}
		// Door to Ballroom
		if (Xdes == 7 && Ydes == 19 || Xdes == 9 && Ydes == 16 || Xdes == 14 && Ydes == 16
				|| Xdes == 16 && Ydes == 19) {
			Board[Xdes][Ydes] = new Room(6);

		}

		int xdif, ydif;
		if (Xcurrent > Xdes) {
			xdif = Xcurrent - Xdes;
		} else {
			xdif = Xdes - Xcurrent;
		}
		if (Ycurrent > Ydes) {
			ydif = Ycurrent - Ydes;
		} else {
			ydif = Ydes - Ycurrent;
		}

		if (ydif + xdif <= diceNum && Board[Xdes][Ydes] == "Hallway") {
			return true;
		} else
			return false;
	}

	public void initialize() {
		/**
		 * this loop will make anything without a value a Hallway
		 */
		for (int x = 0; x < Board.length; x++) {
			for (int y = 0; y < Board.length; y++) {
				Board[x][y] = new Hallway();
			}
		}

		/**
		 * Makes a certain amount of "tiles" equal to a room. in other words
		 * this assigns the indexes for Conservatory
		 */
		for (int x = 0; x < 5; x++) {
			for (int y = 20; y < 23; y++) {
				Board[x][y] = new Room();
			}
		}
		Board[1][19] = new Room();
		Board[2][19] = new Room();
		Board[3][19] = new Room();
		Board[4][19] = new Room();

		/**
		 * Makes a certain amount of "tiles" equal to a room. in other words
		 * this assigns the indexes for the Billiards room
		 */

		for (int x = 0; x < 5; x++) {
			for (int y = 12; y < 16; y++) {
				Board[x][y] = new Room();
			}
		}
		/**
		 * Makes a certain amount of "tiles" equal to a room. in other words
		 * this assigns the indexes for the Library
		 */
		for (int x = 1; x < 5; x++) {
			for (int y = 6; y < 10; y++) {
				Board[x][y] = new Room();
			}
		}

		Board[0][7] = new Room();
		Board[0][8] = new Room();
		Board[0][9] = new Room();
		Board[6][7] = new Room();
		Board[6][8] = new Room();
		Board[6][9] = new Room();

		/**
		 * Makes a certain amount of "tiles" equal to a room. in other words
		 * this assigns the indexes for the "Study"
		 */
		for (int x = 0; x < 6; x++) {
			for (int y = 0; y < 3; y++) {
				Board[x][y] = new Room();
			}
		}

		/**
		 * Makes a certain amount of "tiles" equal to a room. in other words
		 * this assigns the indexes for the Hall
		 */
		for (int x = 9; x < 14; x++) {
			for (int y = 0; y < 6; y++) {
				Board[x][y] = new Room();
			}
		}

		/**
		 * Makes a certain amount of "tiles" equal to a room. in other words
		 * this assigns the indexes for the Lounge
		 */
		for (int x = 17; x < 23; x++) {
			for (int y = 0; y < 5; y++) {
				Board[x][y] = new Room();
			}
		}

		/**
		 * Makes a certain amount of "tiles" equal to a room. in other words
		 * this assigns the indexes for the Dining room
		 */
		for (int x = 16; x < 23; x++) {
			for (int y = 9; y < 14; y++) {
				Board[x][y] = new Room();
			}
		}

		Board[19][15] = new Room();
		Board[20][15] = new Room();
		Board[21][15] = new Room();
		Board[22][15] = new Room();
		Board[23][15] = new Room();

		/**
		 * Makes a certain amount of "tiles" equal to a room. in other words
		 * this assigns the indexes for the Kitchen
		 */
		for (int x = 18; x < 23; x++) {
			for (int y = 19; y < 23; y++) {
				Board[x][y] = new Room();
			}
		}

		Board[18][18] = new Room();
		Board[19][18] = new Room();
		Board[20][18] = new Room();
		Board[21][18] = new Room();
		Board[22][18] = new Room();

		/**
		 * Makes a certain amount of "tiles" equal to the rectangle that you put
		 * the deck of cards on so it will be null because not a valid move tile
		 */
		for (int x = 9; x < 13; x++) {
			for (int y = 8; y < 14; y++) {
				Board[x][y] = null;
			}
		}

		/**
		 * this sets the shape of the rooms making some tiles null because the
		 * rooms aren't perfect squares and this gives it its shape
		 */
		Board[0][24] = null;
		Board[1][24] = null;
		Board[2][24] = null;
		Board[3][24] = null;
		Board[4][24] = null;
		Board[5][24] = null;
		Board[6][24] = null;
		Board[7][24] = null;
		Board[8][24] = null;
		Board[15][24] = null;
		Board[16][24] = null;
		Board[17][24] = null;
		Board[18][24] = null;
		Board[19][24] = null;
		Board[20][24] = null;
		Board[21][24] = null;
		Board[23][24] = null;
		Board[7][23] = null;
		Board[17][23] = null;
		Board[0][19] = null;
		Board[23][18] = null;
		Board[0][17] = null;
		Board[23][16] = null;
		Board[0][11] = null;
		Board[0][10] = null;
		Board[23][8] = null;
		Board[0][6] = null;
		Board[23][6] = null;
		Board[0][4] = null;
		Board[8][0] = null;
		Board[15][0] = null;
		Board[17][0] = null;
	}
		
	
	public Object getPos(int newX, int newY){
		Object  position = Board[newX][newY];
		return position;
	}
	
	
	
	public Object getPos(int input){
		
		switch(input){
		case 1: curPos = Board[Xdes][Ydes];//Col_Musturd_CurPos
		Xdes = 23;
		Ydes = 7;
		
		break;
		case 2:curPos = Board[Xdes][Ydes];//Mr_Green_CurPos
		Xdes = 9;
		Ydes = 24;
		break;
		case 3: curPos = Board[Xdes][Ydes];//Mrs_White_CurPos
		Xdes = 14;
		Ydes = 24;
		break;
		case 4: curPos = Board[Xdes][Ydes];//Mrs_Peacock_CurPos
		Xdes = 0;
		Ydes = 18;
		break;
		case 5: curPos = Board[Xdes][Ydes];//Miss_Scarlet_CurPos
		Xdes = 16;
		Ydes = 0;
		break;
		case 6: curPos = Board[Xdes][Ydes];//Prof_Plum_CurPos
		Xdes = 0;
		Ydes = 5;
		break;
	
	}
		return curPos;

}
}
