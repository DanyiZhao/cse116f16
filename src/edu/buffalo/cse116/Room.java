package edu.buffalo.cse116;

/**
 * this class will set the room names and assign a number for us to reference in
 * other classes. the number given is Final which means that the value is
 * constant and can not be changed
 * 
 * @author Erick Cisneros
 */
public class Room {
	public static final int HALL = 0;
	public static final int LOUNGE = 1;
	public static final int DINING = 2;
	public static final int KITCHEN = 3;
	public static final int BALLROOM = 4;
	public static final int CONSERVATORY = 5;
	public static final int BILLIARD = 6;
	public static final int LIBRARY = 7;
	public static final int STUDY = 8;
    public int DoorNum;
	
    public Room() {
		
	}
	public Room(int num){
		DoorNum =num;
	}

	public boolean Roomequals(Object obj) {
		return (obj instanceof Room);
	}
	
	public String getRoom(){
		String room;
        switch (DoorNum) {
           
            case 0:  room = "Hall";
            		 break; 
            case 1:  room = "Lounge";
                     break;
            case 2:  room = "Dining Room";
                     break;
            case 3:  room = "Kitchen";
                     break;
            case 4:  room = "Ballroom";
                     break;
            case 5:  room = "Conservatory";
                     break;
            case 6:  room = "Billiard";
                     break;
            case 7:  room = "Library";
                     break;
            case 8:  room = "Study";
                     break;
            default: room = "Invalid room";
                     break;
        }
        System.out.println(room);
        return room;
        
    }
}
	
	
	
	
	
	
	