package teeest;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.util.Random;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

public class AbsoluteLayoutExample extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public AbsoluteLayoutExample() {
		super("Clude Alpha0.1");// titles
		
		this.setVisible(true);
		this.setSize(600, 600);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);

		JPanel panel = new JPanel(new GridBagLayout());
		//JPanel panel2 = new JPanel();// Haven't use
		JPanel imagepanel = new JPanel(null);
		JPanel suggestionp = new JPanel(null);

		ImageIcon upimage = new ImageIcon("Image/up.jpg");
		ImageIcon downimage = new ImageIcon("Image/down.jpg");
		ImageIcon leftimage = new ImageIcon("Image/left.jpg");
		ImageIcon rightimage = new ImageIcon("Image/right.jpg");
		ImageIcon simage = new ImageIcon("Image/s.jpg");

		JButton up = new JButton(upimage);
		up.setPreferredSize(new Dimension(25, 25));

		JButton down = new JButton(downimage);
		JButton left = new JButton(leftimage);
		JButton right = new JButton(rightimage);
		JButton suggestion = new JButton(simage);
		JLabel player = new JLabel("player");

		ImageIcon board = new ImageIcon("Image/board.jpg");

		JLabel imLabel = new JLabel(board);
		imLabel.setPreferredSize(new Dimension(board.getIconWidth(), board.getIconHeight()));
		JButton player1 = new JButton();
		player1.setBackground(Color.BLACK);

		JButton player2 = new JButton("B");
		player2.setBackground(Color.BLUE);
		JButton player3 = new JButton("C");
		player3.setBackground(Color.GREEN);
		JButton player4 = new JButton("D");
		player4.setBackground(Color.WHITE);
		JButton player5 = new JButton("E");
		player5.setBackground(Color.YELLOW);
		JButton player6 = new JButton("F");
		player6.setBackground(Color.RED);
		//
		JButton Miss_Peacock = new JButton("Miss Peacock");
		JButton Knife = new JButton("Knife");
		suggestionp.add(Miss_Peacock);
		suggestionp.add(Knife);
		
		
		//
		GridBagConstraints Button = new GridBagConstraints();
		Button.gridx = 2;
		Button.gridy = 0;
		Button.ipadx = 0;

		panel.add(up, Button);
		Button.gridx = 2;
		Button.gridy = 1;
		Button.ipadx = 0;

		panel.add(down, Button);
		Button.gridx = 1;
		Button.gridy = 1;
		Button.ipadx = 0;

		panel.add(left, Button);
		Button.gridx = 3;
		Button.gridy = 1;
		Button.ipadx = 0;

		panel.add(right, Button);
		Button.gridx = 4;
		Button.gridy = 1;

		panel.add(suggestion, Button);
		Button.gridx = 7;
		Button.gridy = 0;

		panel.add(player, Button);

		imagepanel.add(imLabel);
		imLabel.setBounds(-10, -130, 800, 900);
		imagepanel.add(player1);
		player1.setBounds(50, 145, 25, 25);
		imagepanel.add(player2);
		player2.setBounds(50, 470, 25, 25);
		imagepanel.add(player3);
		player3.setBounds(533, 15, 25, 25);
		imagepanel.add(player4);
		player4.setBounds(473, 620, 25, 25);
		imagepanel.add(player5);
		player5.setBounds(320, 620, 25, 25);
		imagepanel.add(player6);
		player6.setBounds(753, 193, 25, 25);

		this.add(panel, BorderLayout.SOUTH);

		this.add(imagepanel);

		// GRIDING

		SwingUtilities.updateComponentTreeUI(this);
		
		Knife.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				test7();
			}

			private void test7() {
				System.out.println("You Pressed down");

			}

		});

		Miss_Peacock.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				Knife.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {

						test7();
					}

					private void test7() {
						System.out.println("You Pressed down");

					}

				});
			}

			//private void test3() {
			//	System.out.println("You Pressed down");

		//	}

		});
		
		
		up.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				diceRoll();
			}

			public int diceRoll() {
				Random rand = new Random();

				int diceNum = rand.nextInt(11) + 2;
				// 12 is the maximum and the 2 is our minimum
				
				System.out.println("You rolled  "+diceNum);
			return diceNum;}

		});
		down.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				test3();
			}

			private void test3() {
				System.out.println("You Pressed down");

			}

		});

		left.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				test4();
			}

			private void test4() {
				System.out.println("You Pressed left");

			}

		});
		
		right.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				test5();
			}

			private void test5() {
				System.out.println("You Pressed right");

			}

		});
		
		suggestion.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				System.out.println("You Pressed Suggestion");

			}
		});
	}

	public void test() {
		System.out.println("Print this as a test");
	}

	}


