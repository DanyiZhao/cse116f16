package teeest;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
 
public class Suggestion {
    final static boolean shouldFill = true;
    final static boolean shouldWeightX = true;
    final static boolean RIGHT_TO_LEFT = false;
    private static String weaponCard;
    private static String roomCard;
    private static String personCard;
   
 
    public static void addComponentsToPane(Container pane) {
        if (RIGHT_TO_LEFT) {
            pane.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
        }
 
        JButton button;
    pane.setLayout(new GridBagLayout());
    GridBagConstraints c = new GridBagConstraints();
    if (shouldFill) {
    //natural height, maximum width
    c.fill = GridBagConstraints.HORIZONTAL;
    }
 
    JComboBox person = new JComboBox();
	person.addItem("people");
	person.addItem("Miss Scarlet");
	person.addItem("Mr. Green");
	person.addItem("Mrs. Peacock");
	person.addItem("Colonel Mustard");
	person.addItem("Mrs. White");
	person.addItem("Professor Plum");
	person.setBackground(Color.red);
	person.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            if (person.getSelectedItem().toString().equals("Miss Scarlet")){
                System.out.println("Miss Scarlet");
                personCard="Miss Scarlet";
                
	        } else if(person.getSelectedItem().toString().equals("Mr. Green")) {   
	        	System.out.println("Mr. Green");
	        	personCard="Mr. Green";
            } else if(person.getSelectedItem().toString().equals("Mrs. Peacock")) {   
	        	System.out.println("Mrs. Peacock");
	        	personCard="Mrs. Peacock";
            }else if(person.getSelectedItem().toString().equals("Colonel Mustard")) {   
	        	System.out.println("Colonel Mustard");
	        	personCard="Colonel Mustard";
            }else if(person.getSelectedItem().toString().equals("Mrs. White")) {   
		        	System.out.println("Mrs. White");
		        	personCard="Mrs. White";
            }else  {   
	        	System.out.println("Professor Plum");
	        	personCard="Professor Plum";
            }
            
            
       }   
 });
    if (shouldWeightX) {
    c.weightx = 0.5;
    }
    c.fill = GridBagConstraints.HORIZONTAL;
    c.gridx = 0;
    c.gridy = 0;
    pane.add(person, c);
 
    JComboBox room = new JComboBox();
	room.addItem("room");
	room.addItem("Conservatory");
	room.addItem("Kitchen");
	room.addItem("Ballroom");
	room.addItem("Billiard Room");
	room.addItem("Library");
	room.addItem("Study");
	room.addItem("Dining Room");
	room.addItem("Lounge");
	room.addItem("Hall");
	room.setBackground(Color.orange);
	room.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            if (room.getSelectedItem().toString().equals("Conservatory")){
                System.out.println("Conservatory");
                roomCard="Conservatory";
                
	        } else if(room.getSelectedItem().toString().equals("Kitchen")) {   
	        	System.out.println("Kitchen");
	        	roomCard="Kitchen";
            } else if(room.getSelectedItem().toString().equals("Ballroom")) {   
	        	System.out.println("Ballroom");
	        	roomCard="Ballroom";
            }else if(room.getSelectedItem().toString().equals("Billiard Room")) {   
	        	System.out.println("Billiard Room");
	        	roomCard="Billiard Room";
            }else if(room.getSelectedItem().toString().equals("Library")) {   
	        	System.out.println("Library");
	        	roomCard="Library";
	        		
            }else if(room.getSelectedItem().toString().equals("Study")) {   
	        	System.out.println("Study");
	        	roomCard="Study";
	        	
            }else if(room.getSelectedItem().toString().equals("Dining Room")) {   
	        	System.out.println("Dining Room");
	        	roomCard="Dining Room";
	        	
            }else if(room.getSelectedItem().toString().equals("Lounge")) {   
	        	System.out.println("Lounge");
	        	roomCard="Lounge";		
            }else  {   
	        	System.out.println("Wrench");
	        	roomCard="Wrench";
            }
            
            
       }   
 });
    c.fill = GridBagConstraints.HORIZONTAL;
    c.weightx = 0.5;
    c.gridx = 1;
    c.gridy = 0;
    pane.add(room, c);
 
    JComboBox weapon = new JComboBox();
	weapon.addItem("weapon");
	weapon.addItem("Candlestick");
	weapon.addItem("Knife");
	weapon.addItem("Rope");
	weapon.addItem("Revolver");
	weapon.addItem("Lead Pipe");
	weapon.addItem("Wrench");
	weapon.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            if (weapon.getSelectedItem().toString().equals("Candlestick")){
                System.out.println("Candlestick");
                weaponCard="Candlestick";
                
	        } else if(weapon.getSelectedItem().toString().equals("Knife")) {   
	        	System.out.println("Knife");
                weaponCard="Knife";
            } else if(weapon.getSelectedItem().toString().equals("Rope")) {   
	        	System.out.println("Rope");
                weaponCard="Rope";
            }else if(weapon.getSelectedItem().toString().equals("Revolver")) {   
	        	System.out.println("Revolver");
                weaponCard="Revolver";
            }else  {   
	        	System.out.println("Wrench");
                weaponCard="Wrench";
               
            } 
           
            
            
       } 
 });
	//printAll();	
	//System.out.println(weapon.);
	
	room.setBackground(Color.green);
    c.fill = GridBagConstraints.HORIZONTAL;
    c.weightx = 0.5;
    c.gridx = 2;
    c.gridy = 0;
    pane.add(weapon, c);
    }
    
   
    
   
 
    private static void createAndShowGUI() {
        //Create and set up the window.
        JFrame frame = new JFrame("GridBagLayoutDemo");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
 
        //Set up the content pane.
        addComponentsToPane(frame.getContentPane());
 
        //Display the window.
        frame.pack();
        frame.setVisible(true);
    }
    
    
    public static void printAll(){
    	System.out.println(personCard+weaponCard+roomCard);
    }
    
 
    public static void main(String[] args) {
        //Schedule a job for the event-dispatching thread:
        //creating and showing this application's GUI.
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                createAndShowGUI();
            }
        });
        
        
    }
    
    
  //  
}